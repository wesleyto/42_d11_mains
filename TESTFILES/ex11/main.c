/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wto <marvin@42.fr>                         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/22 13:48:20 by wto               #+#    #+#             */
/*   Updated: 2017/08/22 13:57:11 by wto              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>
#include "ft_list_find.c"
#include <string.h>
#include <unistd.h>

t_list	*ft_list_push_params(int ac, char **av);

int main(void)
{
	t_list *list;
	char *str0 = "0";
	char *str1 = "1";
	char *str2 = "2";
	char *str3 = "3";
	char *str4 = "4";
	char *str5 = "5";
	char *strs[] = {str0, str0, str1, str2, str3, str4, str5};
	list = ft_list_push_params(7, strs);
	char *cases[] = {str0, str1, str2, str3, str4, str5};
	char *res_str = "%s || Case: %-3s || Exp: %-8s || Got: %-8s\n";

	for (int i = 0; i < 6; i++)
	{
		t_list *result = ft_list_find(list, cases[i], &strcmp);
		char *res = strcmp(cases[i], result->data) == 0 ? "Success" : "Failure";
		printf(res_str, res, cases[i], cases[i], result->data);
	}
	t_list *result = ft_list_find(list, "6", &strcmp);
	char *res = result == NULL ? "Success" : "Failure";
	printf(res_str, res, "6", NULL, result ? result->data : result);
}
