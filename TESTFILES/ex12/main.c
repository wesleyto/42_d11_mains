/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wto <marvin@42.fr>                         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/22 13:48:20 by wto               #+#    #+#             */
/*   Updated: 2017/08/22 13:57:11 by wto              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>
#include "ft_list_remove_if.c"
#include <string.h>
#include <unistd.h>

t_list	*ft_list_push_params(int ac, char **av);

void print_list(t_list *list)
{
	printf("%s", "(");
	while (list)
	{
		if (list->next)
		{
			printf("%s->", list->data);
		}
		else
		{
			printf("%s", list->data);
		}
		list = list->next;
	}
	printf("%s", ")");
}

int main(void)
{
	t_list *list;
	char *str0 = "0";
	char *str1 = "1";
	char *str2 = "2";
	char *str3 = "3";
	char *str4 = "4";
	char *str5 = "5";
	char *strs[] = {str0, str0, str1, str2, str3, str4, str5,
					str0, str1, str2, str3, str4, str5,
					str5, str4, str3, str2, str1, str0};
	char *cases[] = {str0, str4, str3, str2, str1, str5};
	list = ft_list_push_params(19, strs);
	printf("Initial State:%s", "\n\t");
	print_list(list);
	for (int i = 0; i < 6; i++)
	{
		char *str = cases[i];
		printf("\nRemoving %s:\n\t", str);
		ft_list_remove_if(&list, str, &strcmp);
		print_list(list);
	}
}
