/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wto <marvin@42.fr>                         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/22 12:16:01 by wto               #+#    #+#             */
/*   Updated: 2017/08/22 12:20:01 by wto              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "ft_create_elem.c"

int main(void)
{
	t_list *elem;
	char *str = "Success! Something";
	char *str2 = "Success! Something else";
	elem = ft_create_elem(str);
	printf("\"%s\" is stored in the listnode\n", (*elem).data);
	elem = ft_create_elem(str2);
	printf("\"%s\" is stored in the listnode\n", (*elem).data);
	free(elem);
}
