/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wto <marvin@42.fr>                         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/22 13:48:20 by wto               #+#    #+#             */
/*   Updated: 2017/08/22 13:57:11 by wto              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>
#include "ft_list_merge.c"
#include <string.h>
#include <unistd.h>

t_list	*ft_list_push_params(int ac, char **av);

void print_list(t_list *list)
{
	printf("%s", "(");
	while (list)
	{
		if (list->next)
		{
			printf("%s->", list->data);
		}
		else
		{
			printf("%s", list->data);
		}
		list = list->next;
	}
	printf("%s", ")");
}

int main(void)
{
	t_list *list1;
	t_list *list2;
	char *str0 = "0";
	char *str1 = "1";
	char *str2 = "2";
	char *str3 = "3";
	char *str4 = "4";
	char *str5 = "5";
	char *l1_1[] = {str0, str0, str1, str2};
	char *l1_2[] = {str0, str3, str4, str5};
	char *l2_1[] = {str0};
	char *l2_2[] = {str0, str1, str2, str3};
	char *l3_1[] = {str0, str0, str1, str2};
	char *l3_2[] = {str0};
	char len1[] = {4, 1, 4};
	char len2[] = {4, 4, 1};


	char **list1s[] = {l1_1, l2_1, l3_1};
	char **list2s[] = {l1_2, l2_2, l3_2};

	for (int i = 0; i < 3; i++)
	{
		list1 = ft_list_push_params(len1[i], list1s[i]);
		list2 = ft_list_push_params(len2[i], list2s[i]);
		printf("List 1:%s", "\t");
		print_list(list1);
		printf("\nList 2:%s", "\t");
		print_list(list2);
		ft_list_merge(&list1, list2);
		printf("\nMerged:%s", "\t");
		print_list(list1);
		printf("\n%s", "\n");
	}
}
