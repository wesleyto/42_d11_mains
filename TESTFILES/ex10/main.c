/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wto <marvin@42.fr>                         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/22 13:48:20 by wto               #+#    #+#             */
/*   Updated: 2017/08/22 13:57:11 by wto              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>
#include "ft_list_foreach_if.c"
#include <string.h>
#include <unistd.h>

t_list	*ft_list_push_params(int ac, char **av);

void putstr(void *str)
{
	char *ptr;
	ptr = (char *)str;
	printf("Got: %-3s", ptr);
}

int main(void)
{
	t_list *list;
	char *str0 = "0";
	char *str1 = "1";
	char *str2 = "2";
	char *str3 = "3";
	char *str4 = "4";
	char *str5 = "5";
	char *strs[] = {str0, str0, str1, str2, str3, str4, str5};
	list = ft_list_push_params(7, strs);
	char *cases[] = {str0, str1, str2, str3, str4, str5};

	for (int i = 0; i < 6; i++)
	{
		ft_list_foreach_if(list, &putstr, cases[i], &strcmp);
		printf(" || Exp: %-3s\n", cases[i]);
	}
	printf("Got: %s", "   ");
	ft_list_foreach_if(list, &putstr, "6", &strcmp);
	printf(" || Exp: %-3s\n", "");
}
