/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wto <marvin@42.fr>                         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/22 13:48:20 by wto               #+#    #+#             */
/*   Updated: 2017/08/22 13:57:11 by wto              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>
#include "ft_list_push_params.c"

int main(void)
{
	int argc = 6;
	char *str0 = "";
	char *str1 = "reverse.";
	char *str2 = "in ";
	char *str3 = "is ";
	char *str4 = "This ";
	char *str5 = "Success! ";
	char *argv[] = {str0, str1, str2, str3, str4, str5};
	t_list *list;

	list = ft_list_push_params(argc, argv);
	while (list)
	{
		printf("%s ", list->data);
		list = list->next;
	}
	if (argc > 1)
		printf("%s", "\n");
	return (0);
}
