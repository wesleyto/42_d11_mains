/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wto <marvin@42.fr>                         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/22 13:29:48 by wto               #+#    #+#             */
/*   Updated: 2017/08/22 13:47:27 by wto              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "ft_list_last.c"

void	ft_list_push_back(t_list **begin_list, void *data);

int main(void)
{
	t_list *list;
	list = NULL;
	char *str0 = "Str0";
	char *str1 = "Str1";
	char *str2 = "Str2";
	char *str3 = "Str3";
	char *str4 = "Str4";
	char *str5 = "Str5";
	char *str6 = "Str6";
	char *strs[] = {str0, str1, str2, str3, str4, str5, str6};
	char *res_str = "%s || Exp: %-6s || Got: %-6s\n";
	t_list *result = ft_list_last(list);
	printf(res_str, result == NULL ? "Success" : "Failure", NULL, result ? result->data : result);
	for (int i = 0; i < 7; i++)
	{
		ft_list_push_back(&list, strs[i]);
		result = ft_list_last(list);
		printf(res_str, strcmp(strs[i], result->data) == 0 ? "Success" : "Failure", strs[i], result->data);
	}
	return (0);
}

