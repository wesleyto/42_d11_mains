/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wto <marvin@42.fr>                         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/22 13:13:59 by wto               #+#    #+#             */
/*   Updated: 2017/08/22 13:28:15 by wto              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include <stdlib.h>
#include "ft_list_size.c"
#include <stdio.h>

t_list *ft_create_elem(void *data);

int main(void)
{
	t_list *list;
	t_list *temp;
	int result;
	char *str0 = "0";
	char *str1 = "1";
	char *str2 = "2";
	char *str3 = "3";
	char *str4 = "4";
	char *str5 = "5";
	char *str6 = "6";
	char *str7 = "7";
	char *str8 = "8";
	char *str9 = "9";
	char *strs[] = {str0, str1, str2, str3, str4, str5, str6, str7, str8, str9};
	char *res_str = "%s || Got: %-2d || Exp: %-2d\n";
	list = NULL;
	result = ft_list_size(list);
	printf(res_str, result == 0 ? "Success" : "Failure", result, 0);
	for (int i = 0; i < 10; i++)
	{
		if (!list)
		{
			list = ft_create_elem(strs[i]);
			result = ft_list_size(list);
			printf(res_str, result == i + 1 ? "Success" : "Failure", result, i + 1);
		}
		else
		{
			temp = list;
			list = ft_create_elem(strs[i]);
			list->next = temp;
			result = ft_list_size(list);
			printf(res_str, result == i + 1 ? "Success" : "Failure", result, i + 1);
		}
	}
	return (0);
}
