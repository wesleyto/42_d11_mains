/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wto <marvin@42.fr>                         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/22 12:20:46 by wto               #+#    #+#             */
/*   Updated: 2017/08/22 13:01:01 by wto              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list_push_back.c"
#include <stdio.h>
#include <stdlib.h>

t_list *ft_create_elem(void *data);

int main(void)
{
	t_list *listnode;
	t_list *listnode2;
	t_list **ln1;
	t_list **ln2;
	char *str1 = "Success! ";
	char *str2 = "This Sentence ";
	char *str3 = "Only ";
	char *str4 = "Makes ";
	char *str5 = "Sense ";
	char *str6 = "In Order.";
	char *str7 = "\n";
	char *str8 = "Initially NULL List Handled.";
	char *strs[] = {str2, str3, str4, str5, str6, str7};


	listnode = ft_create_elem(str1);
	ln1 = &listnode;
	for (int i = 0; i < 6; i++)
	{
		ft_list_push_back(ln1, strs[i]);
	}
	t_list *temp;
	temp = listnode;
	while (temp)
	{
		printf("%s", temp->data);
		temp = temp->next;
	}
	temp = listnode;
	while (temp)
	{
		t_list *orphan = temp;
		temp = temp->next;
		free(orphan);
	}


	listnode2 = NULL;
	ln2 = &listnode2;
	ft_list_push_back(ln2, str1);
	ft_list_push_back(ln2, str8);
	ft_list_push_back(ln2, str7);
	temp = listnode2;
	if (temp)
	{
		while (temp)
		{
			printf("%s", temp->data);
			temp = temp->next;
		}
	}
	else
	{
		printf("Failure! Initially NULL list not handled%s", "\n");
	}

	temp = listnode2;
	while (temp)
	{
		t_list *orphan = temp;
		temp = temp->next;
		free(orphan);
	}
	
	return (0);
}











