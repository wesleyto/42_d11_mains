/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wto <marvin@42.fr>                         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/22 13:48:20 by wto               #+#    #+#             */
/*   Updated: 2017/08/22 13:57:11 by wto              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>
#include "ft_list_foreach.c"
#include <string.h>
#include <unistd.h>

t_list	*ft_list_push_params(int ac, char **av);

void putstr(void *str)
{
	char *ptr;
	ptr = (char *)str;
	while (*ptr)
	{
		write(1, ptr, 1);
		ptr++;
	}
}

int main(void)
{
	t_list *list;
	char *str0 = "0";
	char *str1 = "1";
	char *str2 = "2";
	char *str3 = "3";
	char *str4 = "4";
	char *str5 = "5";
	char *strs[] = {str0, str0, str1, str2, str3, str4, str5};
	list = ft_list_push_params(7, strs);
	putstr("Got:\t");
	ft_list_foreach(list, &putstr);
	printf(" || Exp:\t543210%s", "\n");
}
