/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wto <marvin@42.fr>                         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/22 13:48:20 by wto               #+#    #+#             */
/*   Updated: 2017/08/22 13:57:11 by wto              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>
#include "ft_list_sort.c"
#include <string.h>
#include <unistd.h>

t_list	*ft_list_push_params(int ac, char **av);

void print_list(t_list *list)
{
	printf("%s", "(");
	while (list)
	{
		if (list->next)
		{
			printf("%s->", list->data);
		}
		else
		{
			printf("%s", list->data);
		}
		list = list->next;
	}
	printf("%s", ")");
}

int main(void)
{
	t_list *list;
	char *str0 = "0";
	char *str1 = "1";
	char *str2 = "2";
	char *str3 = "3";
	char *str4 = "4";
	char *str5 = "5";
	char *l1[] = {str0, str2, str4, str3, str5, str1, str0};
	char *l2[] = {str0, str2, str4, str3, str5, str1, str0, str5, str2, str4};
	char *l3[] = {str0, str0};
	char *l4[] = {str0};
	char len[] = {7, 10, 2, 1};


	char **lists[] = {l1, l2, l3, l4};

	for (int i = 0; i < 4; i++)
	{
		list = ft_list_push_params(len[i], lists[i]);
		printf("Before:%s", "\t");
		print_list(list);
		ft_list_sort(&list, &strcmp);
		printf("\nAfter:%s", "\t");
		print_list(list);
		printf("\n%s", "\n");
	}
}
