/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wto <marvin@42.fr>                         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/22 13:48:20 by wto               #+#    #+#             */
/*   Updated: 2017/08/22 13:57:11 by wto              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>
#include "ft_list_reverse_fun.c"
#include <string.h>

t_list	*ft_list_push_params(int ac, char **av);
t_list	*ft_list_at(t_list *begin_list, unsigned int nbr);

int main(void)
{
	t_list *list;
	t_list *curr;
	char *res_str = "%s || Got: %-8s || Exp: %-8s || Case: Position %d\n";
	char *res;
	char *str0 = "0";
	char *str1 = "1";
	char *str2 = "2";
	char *str3 = "3";
	char *str4 = "4";
	char *str5 = "5";
	char *strs[] = {str0, str0, str1, str2, str3, str4, str5};
	char *exps[] = {str0, str1, str2, str3, str4, str5};
	list = ft_list_push_params(7, strs);
	ft_list_reverse_fun(list);
	for (int i = 0; i < 6; i++)
	{
		curr = ft_list_at(list, i);
		res = strcmp(curr->data, exps[i]) == 0 ? "Success" : "Failure";
		printf(res_str, res, curr->data, exps[i], i);
	}
}
