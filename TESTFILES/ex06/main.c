/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wto <marvin@42.fr>                         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/22 13:48:20 by wto               #+#    #+#             */
/*   Updated: 2017/08/22 13:57:11 by wto              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>
#include "ft_list_clear.c"

t_list	*ft_list_push_params(int ac, char **av);

void print_list(t_list *list)
{
	printf("%s", "(");
	while (list)
	{
		if (list->next)
		{
			printf("%s->", list->data);
		}
		else
		{
			printf("%s", list->data);
		}
		list = list->next;
	}
	printf("%s", ")");
}

int main(void)
{
	t_list *list;
	t_list **ln1;
	char *str0 = "0";
	char *str1 = "1";
	char *str2 = "2";
	char *str3 = "3";
	char *str4 = "4";
	char *str5 = "5";
	char *strs[] = {str0, str0, str1, str2, str3, str4, str5};


	list = ft_list_push_params(7, strs);
	ln1 = &list;

	printf("Initial State:%s", "\t");
	print_list(list);

	ft_list_clear(ln1);

	printf("\nCurrent State:%s", "\t");
	print_list(list);

	if (list == NULL)
	{
		printf("\nSuccess!%s", "\n");
	}
	else
	{
		printf("\nFailure! The list is not NULL%s", "\n");
	}

	printf("%s", "\n");
	return (0);
}
